Pirobox tipster is Drupal module base on Pirobox plugin (http://www.pirolab.it/pirobox)  
and idea of jTipster plugin (http://www.idhana.com).


INSTALLATION
------------
1. Copy Pirobox tipster folder to modules directory.
2. Download file from "http://www.piroboxtipster-module.net.pl/plugin/piroboxtipster-dev.tgz"
   (Pack include a modified Pirobox and jTipster plugin files).
   Unpack and copy "pirobox_css", "tipster_css" folder to "css" folder and "piroBox.js", "tispter.js" file to "js" folder.
3. Copy jQuery update folder to modules directory. Tipster working only with 1.3.+ jQuery version.
4. At admin/build/modules enable the Pirobox tipster and jQuery update module.
5. Enable permissions at admin/user/permissions.
6. Configure the module at admin/settings/pirobox_tipster.
7. Copy cck tpl files from "cck fields to copy" folder to Your theme folder. Remember to change file name in 
   content-field-[field name].tpl.php where [field name] is Your cck imageField name. Clear cache.
   More information about theme cck field: http://drupal.org/node/269319.




ADDING TIPSTER TO YOUR IMAGES
--------------------------------------------
To add tipster You have to enable Description field and put code, which look like : description[Width,Height] (position tipster) 
example: Lorem ipsum dolor sit amet, consectetur adipiscing elit. [140,34]
If You don't want to use tipster left the Description field empty.

The maximum number of tispters per one image is eight.
example: Lorem ipsum dolor sit amet, consectetur adipiscing elit. [140,34];  Lorem ipsum dolor sit amet[22,12];
         Lorem ipsum dolor si [10,34]
         
Don't use at the end ";". Use only as a separator between each tip.

HOW TO USE IFRAME
--------------------------------------------
example:
 <div class="pirobox_field">
     <ul class="thumbs_all">
							<li>
									<a rel="group"   href="http://localhost/drupal/user/register?pb_iframe&width=600&height=400">
										<img height="100" width="100" title="Lorem ipsum" alt="" src="/drupal_cwicz/sites/default/files/imagecache/ere/vbbv.jpg"/>
									</a>
							</li>
						<li>
									<a title="Lorem ipsum" rel="group"  href="http://localhost/drupal/node?pb_iframe='sd'&width=1000&height=1000">
										fgfd
									</a>
							</li>
     </ul>
   </div>