<?php

/**
 * @file
 * Administrative page callbacks for the Pirobox tipster module.
 */ 
function pirobox_tipster_general_settings_form() {

  $form['pirobox_tipster_imagefield'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image field options (CCK)')
  );
  $form['pirobox_tipster_imagefield']['pirobox_tipster_gallery'] = array(
    '#type' => 'radios',
    '#title' => t('Gallery'),
    '#default_value' => variable_get('pirobox_tipster_gallery', 1),
    '#options' =>  array(0 => t('No gallery'), 1 => t('Gallery per field')),
  );
  $form['pirobox_tipster_imagefield']['pirobox_tipster_imagefield_use_node_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use node title as caption'),
    '#description' => t('By default, the caption for imagefields is the image title text configured.  This option allows you to override that and always display the node title as the image caption.'),
    '#default_value' => variable_get('pirobox_tipster_imagefield_use_node_title', FALSE),
  ); 
  $form['advanced_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced_options']['pirobox_tipster_skin_color'] = array(
    '#type' => 'radios',
    '#title' => t('Skin color'),
    '#options' =>  array('white' => t('White'), 'black' => t('Black')),
    '#default_value' => variable_get('pirobox_tipster_skin_color', 'white'),
  );
  $form['advanced_options']['pirobox_tipster_open_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Open speed'),
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => variable_get('pirobox_tipster_open_speed', 1000),
  );
  $form['advanced_options']['pirobox_tipster_close_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Close speed'),
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => variable_get('pirobox_tipster_close_speed', 1000),
  );
  $form['advanced_options']['pirobox_tipster_prev_next_location'] = array(
    '#type' => 'radios',
    '#title' => t('Previous  and next button location'),
    '#options' =>  array('in' => t('In'), 'out' => t('Out')),
    '#default_value' => variable_get('pirobox_tipster_prev_next_location', 'in'),
  );
  $form['advanced_options']['pirobox_tipster_overlay_opacity'] = array(
    '#type' => 'textfield',
    '#title' => t('Overlay opacity'),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Determines how visible the background page is behind the Pirobox box.  The opacity value can range from 0.0 to 1. Decimal number with "." or "," for floating point.'),
    '#default_value' => variable_get('pirobox_tipster_overlay_opacity', '0.8'),
  );

  return system_settings_form($form);
}
