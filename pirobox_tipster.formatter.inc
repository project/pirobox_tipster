<?php

/**
 * @file
 * Pirobox tipster formatter hooks and callbacks.
 */
function theme_pirobox_tipster_formatter_imagefield($element) {
  
  $field_name = $element['#field_name'];
  $item = $element['#item'];
  $formatter = $element['#formatter'];
  $node = node_load($element['#item']['nid']);

  $presets = explode('_', $formatter);
  return pirobox_tipster_imagefield_image_imagecache($field_name, $item, $formatter, $node, $presets);
}


function pirobox_tipster_imagefield_image_imagecache($field_name, $item, $formatter, $node, $presets) {

  $item_path = $item['filepath'];
  if (!isset ($item_path) && isset ($item['fid'])) {
    $file = field_file_load($item['fid']);
    $item_path = $file['filepath'];
  }
  elseif (!isset ($item_path)) {
    // No file in field, return.
    return '';
  }

  $item_data = $item['data'];
  $image_title = (!empty($image_title) ? $image_title : $item_data['title']);
  if (variable_get('pirobox_tipster_imagefield_use_node_title', FALSE)) {
    $image_title = $node->title;
  }
  
  $preset_from = $presets[3];
  $preset_to = $presets[4];

  if ($preset_from == 'original') {
    $link_path_from = file_create_path($item_path);
  }
  else {
    $link_path_from = imagecache_create_path($preset_from, $item_path);
  }

  if ($preset_to == 'original') {
    $link_path_to = file_create_path($item_path);
  } 
  else {
    $link_path_to = imagecache_create_path($preset_to, $item_path);
  }

  $attributes = array('title' => $image_title, 'alt' => $item_data['alt'], 'description' => $item_data['description']);
  return theme('image_pirobox_tipster', $link_path_from, $link_path_to, $attributes);
}


function theme_image_pirobox_tipster($link_path_from, $link_path_to, $attributes) {

  $image =  theme('image', $link_path_from, $attributes['alt']);
  $image .= '<span class="pirobox_tip">'. check_plain($attributes['description']) .'</span>';
  return l($image, $link_path_to, array('html' => TRUE, 'attributes' => array('title' => $attributes['title'])));
 
}
