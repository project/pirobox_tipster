Drupal.behaviors.initPirobox_tipster = function(){

    var settings = Drupal.settings.pirobox_tipster;
    var prev_next_in;
    
    if (settings.prev_next_location == 'in') {
        $('.pirobox_field').addClass('pirobox_in');
        prev_next_in = '_in';
    }
    else 
        if (settings.prev_next_location == 'out') {
            $('.pirobox_field').addClass('pirobox');
            prev_next_in = '';
        }  
		
var gallery = settings.gallery;
    $('.thumbs').piroBox({
        bg_alpha: settings.overlayOpacity.replace(/,/, "."),
      		gallery_option: settings.galleryOption,
        gallery: '.pirobox' + prev_next_in + ' li a',
        gallery_li: '.pirobox' + prev_next_in + ' li',
        next_class: '.next' + prev_next_in + '',
        previous_class: '.previous' + prev_next_in + '',
        open_speed: settings.openSpeed,
        close_speed: settings.closeSpeed
    });   
};


